﻿using UnityEngine;
using System.Collections;

public class moveScript : MonoBehaviour {

	public float minSpeed = 0.8f; 
	public float maxSpeed = 1.2f; 

	public float increase = 0.2f;

	private float speed =1 ;

	Transform t;

	// Use this for initialization
	void Start () {
		t = transform;
		speed = Random.Range (minSpeed, maxSpeed);

		InvokeRepeating ("addSpeed", 10, 10);
	}

	void addSpeed() {
		speed += increase;
	}
	
	// Update is called once per frame
	void Update () {
	
		Vector3 pos = t.localPosition;

		pos.x += speed * Time.deltaTime;

		t.localPosition = pos;

	}
}
