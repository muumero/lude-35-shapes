﻿using UnityEngine;
using System.Collections;

public class smoothFollow : MonoBehaviour {

    public Transform target;

    public float smoothPower = 0.2f;

    private Transform _transform;

	public Transform[] smoothBg;

	public float[] bgSpeedScale;


    void Start()
    {
        _transform = transform;

    }

	// Update is called once per frame
	void Update () {

        if (target)
        {
			float offset = target.position.x - _transform.position.x;

            offset *= smoothPower;

            Vector3 pos = _transform.position;
            pos.x += offset;
            _transform.position = pos;

			//move bg slowly
			Vector3 bgPos = Vector3.zero;
			for ( int n = 0; n < bgSpeedScale.Length;n++) {

				bgPos = smoothBg[n].position;
				bgPos.x = pos.x * bgSpeedScale[n];
				smoothBg[n].position = bgPos;
				
			}

        }

 

	}
}
