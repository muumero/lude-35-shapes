﻿using UnityEngine;
using System.Collections;

public class moveWithCamera : MonoBehaviour {

	private Transform t;
	// Use this for initialization
	void Start () {
		t = transform;
	}
	
	// Update is called once per frame
	void Update () {
	
		float x = Camera.main.transform.position.x;

		if (t.position.x < x -18) {

			Vector3 pos = t.position;
			pos.x = x -17.9f;
			t.position = pos;
		}

	}
}
