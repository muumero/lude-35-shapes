﻿using UnityEngine;
using System.Collections;

public class soundManager : MonoBehaviour {

	public AudioClip jump;
	public AudioClip damage;
	public AudioClip morph;
	public AudioClip passFilter;
	public AudioClip death;

	private AudioSource a;

	void Start() {
		a = GetComponent<AudioSource> ();
	}

	private bool isDead =false;

	public void playSound(string s) {

		switch (s) {
		case "jump":
			a.PlayOneShot(jump);


			break;
		case "death":
			if (!isDead) {
				a.PlayOneShot(death, 0.45f);
				isDead = true;
			}
			
			break;

		case "morph":
			a.PlayOneShot(morph);
			
			
			break;

		}


	}

}
