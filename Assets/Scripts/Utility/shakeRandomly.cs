﻿using UnityEngine;
using System.Collections;

public class shakeRandomly : MonoBehaviour {

	Transform t;

	// Use this for initialization
	void Start () {
	

		t = transform;
		InvokeRepeating ("shake", 0.1f, 0.1f);


	}

	void shake() {
	
		int action = (int)Random.Range (0, 6);

		Vector3 local = t.localScale;

		switch (action) {
		case 0:
			local.x = 2;
			break;
		
		case 1:
			local.x = -2;
			break;
		case 2:
			local.y = 2;
			break;
		case 3:
			local.y = -2;
			break;
		default:
			t.Rotate(new Vector3(0,0,(int)Random.Range(0,4)*90));


			break;
		}

		t.localScale = local;

	}

}
