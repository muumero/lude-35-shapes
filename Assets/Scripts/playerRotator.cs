﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class playerRotator : MonoBehaviour {

	//publics
	public Transform green;
	public Transform orange;
	public Transform bug;

	public LayerMask collisionMask;

	public float collisionDistance = 1;

	public soundManager manager;
		
	public Text score;
	
	public Transform endScreen;
	
	public Material trail_green;
	public Material trail_orange;

	private float bestDistance = 0;
	
	private wallType sideColor = wallType.orange;
	private wallType verticalColor = wallType.orange;
	
	private RaycastHit2D hit;	
	private Transform t;


	// Use this for initialization
	void Start () {
		t = transform;
	}
	
	// Update is called once per frame
	void Update () {

		//original idea was to have different colors in different sides (felt it was too complicated after testing), so this code
		//kinda has some unnecessary features. 

		//rotation
		if (Input.GetButtonDown("Fire2") || Input.GetButtonDown("Fire3")) {			
		/*	rotatingObjects.Rotate(new Vector3(0,0,90));
			wallType temp = sideColor;
			sideColor = verticalColor;
			verticalColor = temp;*/
			manager.playSound("morph");

			if (sideColor == wallType.orange) {

				sideColor = wallType.green;
				verticalColor = wallType.green;

				green.gameObject.SetActive(true);
				orange.gameObject.SetActive(false);

				GetComponent<TrailRenderer>().material = trail_orange;
			} else {

				sideColor = wallType.orange;
				verticalColor = wallType.orange;

				green.gameObject.SetActive(false);
				orange.gameObject.SetActive(true);

				GetComponent<TrailRenderer>().material = trail_green;
			}

		}

		if (t.position.y < -10)
			death ();

		if (t.position.x > bestDistance) {
			bestDistance = t.position.x;
			score.text = "Distance: " + (int)bestDistance;
		}

		//check color collisions
		wallType right, left, up, down;

		right = left = up = down = wallType.neutral;

		//right
		hit = Physics2D.Raycast (t.position, Vector2.right, collisionDistance, collisionMask);
		if (hit) right = hit.collider.gameObject.GetComponent<wallBehaviour> ().type;
		//left
		hit = Physics2D.Raycast (t.position, Vector2.left, collisionDistance, collisionMask);
		if (hit) left = hit.collider.gameObject.GetComponent<wallBehaviour> ().type;
		//up
		hit = Physics2D.Raycast (t.position, Vector2.up, collisionDistance, collisionMask);
		if (hit) up = hit.collider.gameObject.GetComponent<wallBehaviour> ().type;	
		//down
		hit = Physics2D.Raycast (t.position, Vector2.down, collisionDistance, collisionMask);
		if (hit) down = hit.collider.gameObject.GetComponent<wallBehaviour> ().type;

		//if horizontally collides different color than side color
		if (sideColor == wallType.orange) {
			if (right == wallType.green) death();
			if (left == wallType.green) death();
		}
		if (sideColor == wallType.green) {
			if (right == wallType.orange) death();
			if (left == wallType.orange) death();
		}

		//if vertically collides different color than vertical color
		if (verticalColor == wallType.orange) {
			if (up == wallType.green) death();
			if (down == wallType.green) death();
		}
		if (verticalColor == wallType.green) {
			if (up == wallType.orange) death();
			if (down == wallType.orange) death();
		}

		//red always kills
		if (right == wallType.deadly) death();
		if (left == wallType.deadly) death();
		if (up == wallType.deadly) death();
		if (down == wallType.deadly) death();


	}

	void death() {

		endScreen.gameObject.SetActive (true);
		bug.gameObject.SetActive (true);

		green.gameObject.SetActive(false);
		orange.gameObject.SetActive(false);
		
		manager.playSound ("death");

		Destroy(GetComponent<PlayerController>());
		Destroy(this);
	}

}
