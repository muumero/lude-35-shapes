﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class letterAppearing : MonoBehaviour {

	string text;

	Text textField;

	public float speed = 0.05f;

	// Use this for initialization
	void Start () {
	
		textField = GetComponent<Text> ();

		text = textField.text;
		textField.text = "";

		StartCoroutine ("rollText");
	}

	IEnumerator rollText () {
	

		for (int n = 0; n < text.Length; n++) {

			textField.text += text[n];

			yield return new WaitForSeconds(speed);

		}


	}
}
