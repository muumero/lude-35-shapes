﻿using UnityEngine;
using System.Collections;

public class complexRotation : complexMovement {

	public float _angle = 90;
	//private float _startAngle;
	//private Transform _transform;

	public Transform _spinParticles;

	private ParticleSystem _emitter;
	private float _oldAngle = 0;

    public axis axel = axis.z;

	// Use this for initialization
	void Start () {
		base.Start ();
		base.setAngle (_angle);
		_transform = transform;

        if (axel == axis.x) _oldAngle = _startAngle = _transform.rotation.eulerAngles.x;
        if (axel == axis.y) _oldAngle = _startAngle = _transform.rotation.eulerAngles.y;
        if (axel == axis.z) _oldAngle = _startAngle = _transform.rotation.eulerAngles.z;

		if (_spinParticles) {
			_emitter = _spinParticles.GetComponent<ParticleSystem>();
			
		}
	
	}
	
	// Update is called once per frame
	void Update () {
		float locWithEasing = base.calculateLocation ();
		
		//calculate current angle
		float currentAngle = _startAngle + locWithEasing * angle;
		
		
		Vector3 r = transform.rotation.eulerAngles;
        if (axel == axis.x) r.x = currentAngle;
        if (axel == axis.y) r.y = currentAngle;
        if (axel == axis.z) r.z = currentAngle;

		//r.z = currentAngle;
		_transform.rotation = Quaternion.Euler (r);

		if (_spinParticles) {
			float dif = currentAngle - _oldAngle;
			if (dif < 0) dif = -dif;
			_emitter.emissionRate = dif * 20;
			_emitter.startSpeed = dif * 1.5f;

		}
		_oldAngle = currentAngle;
	}

    public enum axis
    {
        x,
        y,
        z

    }
}
